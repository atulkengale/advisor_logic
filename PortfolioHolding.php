<?php


class PortfolioHolding extends utilities {

    public $portfolioid;
    public $SecurityCode;
    public $SecurityDescription;
    public $AssetClassDescription;
    public $AssetType;
    public $Quantity;
    public $UnitCost;
    public $Cost;
    public $UnitValue;
    public $UnitPriceDate;
    public $Value;
    public $PercentTotalValue;
    public $GainOrLoss;
    public $PercentTotalGainOrLoss;
    public $EstIncome;
    public $Yield;
    public $AssetCurrency;
    public $ValueAssetCurrency;
    public $CostBaseUnknown;    
    
    public function savePortfolio($returnHeader,$portfolioId)
    {        
        $portfolios = parent::getArrayFrmJson($returnHeader->data);
        
        extract($portfolios);
        extract($ResponseData);
        extract($Valuation);
        
        if(array_key_exists('Holdings',$ResponseData)){
            
            extract($Holdings);
            
        } else {
            
            return;
        }
        
                
        $this->portfolioid = $portfolioId['InternalID'];
        
        $column = parent::columnFetch('2','PortfolioHolding');           // Coloumn names of table to be fill
        //print_r($column);exit;
        $colcount = count($column);
                
        parent::opendb();
        
        foreach($Holdings as $Holding){
                
                for($i=0; $i<$colcount; $i++){
            
                    $this->$column[$i] = addslashes($Holding[ $column[$i] ]);
            
                }
                
                $this->saveValuationDetail($returnHeader->error);                

        }
        
        parent::closedb();
        
    }
        
    public function saveValuationDetail($qselect){        
        
        if($qselect){
        
            $insert = "INSERT INTO holding_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            // Fetching all coloumn name
            $coloumn = parent::columnFetch('2','PortfolioHolding');

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key
            array_unshift($values,$this->portfolioid);
            array_unshift($coloumn,'portfolio_id');



           $insert = "INSERT INTO holdings (" . implode(', ', $coloumn) . ") ". "VALUES ('" . implode("', '", $values) . "')";
            
        }

        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }
    }
    
    
}


?>