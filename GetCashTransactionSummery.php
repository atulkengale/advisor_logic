<?php

class GetCashTransactionsSummery extends utilities{

    public $CashBookCode;
    public $CashBookDescription;
    public $Currency;
    public $OpeningBalance;
    public $Deposits;
    public $Withdrawals;
    public $ClosingBalance;
    public $ClosingBalancePortfolioCurency;
    public $ClosingForexRate;

    
    public function savePortfolio($returnHeader,$portfolioId)
    {        
        $portfolios = parent::getArrayFrmJson($returnHeader->data);
        
        extract($portfolios);
        extract($ResponseData);
        
        if(array_key_exists('CashBooks',$ResponseData)){
            
            extract($CashBooks); //print_r($AssetClasses);
            
        } else {
            
            return;
        }
                    
        if(count($CashBooks) > 0){
            
            //print_r($CashBooks);
                                
            foreach($CashBooks as $CashBook){
                
                $this->portfolioid = $portfolioId['InternalID'];
        
                $column = parent::columnFetch('4','GetCashTransactionsSummery');           // Coloumn names of table to be fill        
                $colcount = count($column);

                for($i=0; $i<$colcount; $i++){

                    $this->$column[$i] = addslashes($CashBook['Summary'][ $column[$i] ]);

                }

                parent::opendb();

                $this->saveSummary($returnHeader->error);  

                parent::closedb();
                
            }
        } else {
        
            return;
        }        
        
    }
        
    public function saveSummary($qselect){        
        
        if($qselect){
        
            $insert = "INSERT INTO transaction_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            // Fetching all coloumn name
            $coloumn = parent::columnFetch('4','GetCashTransactionsSummery');

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key
            array_unshift($values,$this->portfolioid);
            array_unshift($coloumn,'portfolio_id');



          $insert = "INSERT INTO 
                    get_cash_transaction_summery 
                    (" . implode(', ', $coloumn) . ") ". "VALUES ('" . implode("', '", $values) . "')";
            
        }

        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }
    }
    
}