<?php

class PortfolioDetail extends utilities {
    
    public  $PortfolioDetID;
    public  $PortfolioID;
    public  $PortfolioName;
    public  $ConsolidatedPortfolioType;
    public  $Status;
    public  $RecalcType;
    public  $RecalcStatus;
    public  $ProcessedTo;
    public  $TaxEntityType;
    public  $TotalValue;
    public  $Owner;
    public  $DateCreated;
    
    //Attributes for object Creation 
    
    public $transaction;
    
    
    function savePortfolio($returnHeader,$portfolioId){                //To fetch detail of portfolio
        
        $portfolios = parent::getArrayFrmJson($returnHeader->data);      
        
        extract($portfolios);
        extract($ResponseData);    
        extract($Portfolio);   //print_r($Portfolio);     
        
        $this->portfolioid = $portfolioId['InternalID'];
        
        $column = parent::columnFetch('1','');           // Coloumn names of table to be fill have to declare array position    
        $colcount = count($column);
        
        for($i=0; $i<$colcount; $i++){
          
            $this->$column[$i] = addslashes($Portfolio[ $column[$i] ]);
            
        }
        
        parent::opendb();
            
        $this->saveDetailPortfolio($returnHeader->error);
        
        parent::closedb();
        
        // Call to other Methods Dependant on portfolio Detail and portfolio 
        
        //print_r(array_merge($portfolioId,$Portfolio));exit;
        
        $portfolioDetail = array_merge($portfolioId,$Portfolio);        
        
        array_unshift($portfolioDetail, 0);        
        
        $switcher = new switcher(); 
        
        $switcher->portfolioFetc("other",$portfolioDetail,"3");     //portfolio transaction
        
        $switcher->portfolioFetc("other",$portfolioDetail,"4");     // portfolio cash transaction
        
        $switcher->portfolioFetc("other",$portfolioDetail,"6");     //portfolio performance

            
        date_default_timezone_set("Asia/Kolkata");  

        $date = $portfolioDetail['ProcessedTo'];

        $date = new DateTime($date);

        $date->modify('-30 day');

        $portfolioDetail['StartDate'] = $date->format('Y-m-d');       
        
        $switcher->portfolioFetc("other",$portfolioDetail,"7");      //get daily returns
        
        $switcher->portfolioFetc("other",$portfolioDetail,"11");     //get investment Movement
        
    }
    
    
    public function saveDetailPortfolio($qselect){         
        
        if($qselect){
        
            $insert = "INSERT INTO portfolio_detail_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            $coloumn = parent::columnFetch('1','');

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key
            array_unshift($values,$this->portfolioid);
            array_unshift($coloumn,'portfolio_id');        

            $insert = "INSERT INTO portfolio_detail 
                     (" . implode(', ', $coloumn) . ") ". "VALUES ('" . implode("', '", $values) . "')";
        }
        
        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }          
        
    }
    

}

?>
