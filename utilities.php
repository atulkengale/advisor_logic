<?php

class utilities
{   
    public $con;    
    
    public function opendb(){
    
        $this->con=mysqli_connect("127.0.0.1","root","","portfolios");
            // Check connection
        if (mysqli_connect_errno()){
              echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }  
        
    }    
    
    public function closedb(){
        
        mysqli_close($this->con);
    }
    
    public function getPageCount($config){      
        
        $url = $config['url'];    
        $xml = '<'.$config['payload']['maintag'].'><'.$config['payload']['subtag'].'>'.'*'
                .'</'.$config['payload']['subtag'].'></'.$config['payload']['maintag'].'>';    

        $header = array();
        $header[] = $config['Token'];
        $header[] = $config['contentType']; 

        $page = $this->getportfolios($url,$xml,$header);
        //print_r($page->data);exit;
                
        $jsonstrt = stripos($page->data,'{');
        $jsonend = strlen($page->data);
        $rest = substr($page->data,$jsonstrt,$jsonend);

        $portfolios = json_decode($rest,true);
                
        extract($portfolios);    
        extract($ResponseData);
        extract($PortfolioCount);  
        
        return($PortfolioCount['PageCount']);
        
    }
    
    public function getArrayFrmJson($returnHeader){
    
        $jsonstrt = stripos($returnHeader,'{');
        $jsonend = strlen($returnHeader);
        $rest = substr($returnHeader,$jsonstrt,$jsonend);

        $portfolios = json_decode($rest,true);
        
        return($portfolios);
        
    }
    
    
    public function getportfolios($url,$xml,$header){
        
        $errorhandler = new errorDatahandler();
    
        $httpRequest = curl_init();

        curl_setopt($httpRequest, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($httpRequest, CURLOPT_HTTPHEADER,$header);

        curl_setopt($httpRequest, CURLOPT_POST, 1);

        curl_setopt($httpRequest, CURLOPT_HEADER, 1);

        curl_setopt($httpRequest, CURLOPT_SSL_VERIFYPEER, false);    

        curl_setopt($httpRequest, CURLOPT_URL, $url);

        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $xml);
        //print_r($httpRequest);

        if( ! $errorhandler->data = curl_exec($httpRequest)) 
        { 
            trigger_error(curl_error($httpRequest));
        }
        if( stripos($errorhandler->data,'Server Error') ) {
            //Use macros
            $errorhandler->error = 1;
            
        } else {
            
            $errorhandler->error = 0;
        }
                
        curl_close($httpRequest);    
        
        return $errorhandler;
    
    }
    
    public function readConfigJson(){
        
        $str = file_get_contents('config.json');
        $json = json_decode($str,true);
        
        return($json);
    }
    
    public function getReqrdApiAdd($apiadd){
        
        $json = $this->readConfigJson();
        
        extract($json);
        extract($config);
        extract($dependant);
        
        $array = $dependant[$apiadd];
        
        //foreach($array as $resultarray);print_r($array);exit;
        
        return($array);
    
    }
    
    public function getFirstArray($array){
        
        foreach($array as $reqArray){
        
            $firstArray = $reqArray;
            break;
        }
        //print_r($firstArray);exit;
        
        return($firstArray);
    }
    
    public function columnFetch($position,$specfcPostn){
    
        $json = $this->readConfigJson();
        
        extract($json);
        extract($config);       
        extract($dependant);                
                
        $depPortfolio = $dependant[$position]; //print_r($depPortfolio);exit;
        
        if($specfcPostn != ""){
            
            $returnArray = $depPortfolio[$specfcPostn];
            return($returnArray['coloumns']);
            
        } else {
            foreach($depPortfolio as $reqArray);            
            return($reqArray['coloumns']);
        }
        
        //foreach($depPortfolio as $column);  print_r($column);      
            
                 
    }
    
    public function createXmlQuery($apiarray,$portfolio){
        
        extract($apiarray);
        extract($payload);extract($content);extract($tags);        
        
        $returnString = "";
        
        foreach($tags as $tag){            
            
            $returnString .= $tag['tagname'].''.$portfolio[$tag['value']].''.
                             str_replace("<", "</", $tag['tagname']);
                
        }
               
        return($returnString);
    }
    
    
}


?>