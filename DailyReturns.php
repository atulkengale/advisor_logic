<?php


class DailyReturns extends utilities{

    public $Date;
    public $BMV;
    public $EMV;
    public $NetContribution;
    public $NetInternalTransfers;
    public $TotalExpenses;
    public $TaxCredits;
    public $PercentageReturn;
    public $NetPercentReturn;
    public $CumPercentageReturn;
    public $CumNetPercentReturn;
    
    
    public function savePortfolio($returnHeader,$Portfolio){                 //To fetch child portfolio
                
        $portfolios = parent::getArrayFrmJson($returnHeader->data); 
        
        extract($portfolios);    
        extract($ResponseData);
        
        if(array_key_exists('DailyReturns',$ResponseData)){
            
            extract($DailyReturns);
            
        } else {
            
            return;
        }
                
        $this->portfolioid = $Portfolio['InternalID'];        // id of portfolio
        
        $column = parent::columnFetch('7','');          // Coloumn names of table to be fill       
        $colcount = count($column);        
        
        parent::opendb();
        
        foreach($DailyReturns as $DailyReturn)
        {
            
            for($i=0; $i<$colcount; $i++){
          
            $this->$column[$i] = addslashes($DailyReturn[ $column[$i] ]);
            
            } 
            
            $this->saveDailyReturns($returnHeader->error);
        }
        
        parent::closedb();
        
    }
    
    
    public function saveDailyReturns($qselect){         
        
        if($qselect){
        
            $insert = "INSERT INTO children_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            $coloumn = parent::columnFetch('7','');

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key
            array_unshift($values,$this->portfolioid);
            array_unshift($coloumn,'portfolio_id');


           $insert = "INSERT INTO daily_returns (" . implode(', ', $coloumn) . ") "
                        . "VALUES ('" . implode("', '", $values) . "')";    
            
        }
        
        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }
        
        
        
    }

}


?>