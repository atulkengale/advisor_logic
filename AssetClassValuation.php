<?php

class AssetClassValuation extends utilities{
    
    public $ValuationDate;
    public $TotalCost;
    public $TotalValue;
    public $NetValue;
    public $TotalGainOrLoss;
    public $connectn;

    public function savePortfolio($returnHeader,$portfolioId){
                
        $portfolios = parent::getArrayFrmJson($returnHeader->data);
        
        extract($portfolios);
        extract($ResponseData);
          
        if(array_key_exists('Valuation',$ResponseData)){
            
            extract($Valuation);
            
        } else {
            
            return;
        }
        
            
        $this->portfolioid = $portfolioId['InternalID'];                             // id of portfolio

        $column = parent::columnFetch('5','AssetClassValuation');           // Coloumn names of table to be fill
        $colcount = count($column);         
        
        //print_r($column);exit;
        for($i=0; $i<$colcount; $i++){

            $this->$column[$i] = addslashes($Valuation[ $column[$i] ]);

        }

        parent::opendb(); //print_r($this->connectn);

        $this->saveValuationAsset($returnHeader->error);  

        parent::closedb();
        
    }    
    
        
    public function saveValuationAsset($qselect){       
        
        if($qselect){
        
            $insert = "INSERT INTO asset_class_valuation_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            // Fetching all coloumn name
            $coloumn = parent::columnFetch('5','AssetClassValuation');

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key
            array_unshift($values,$this->portfolioid);
            array_unshift($coloumn,'portfolio_id');


            $insert = "INSERT INTO aeestclass_valuation (" . implode(', ', $coloumn) . ") "
                        . "VALUES ('" . implode("', '", $values) . "')";

        }          
        
        
        if(!mysqli_query($this->con,$insertHistory)){
        
            echo "Sorry Some Error Occured";
        }
        
    }


}

?>
