<?php

class GetInvestmentMovements extends utilities{
    
    public $portfolioid;
    public $FromDate;
    public $ToDate;
    
    public $AssetType;
    public $SecurityCode;
    public $SecurityDescription;
    public $AssetCurrency;
    public $IncludeInPerf;
    public $StartMarketValue;
    public $Purchases;
    public $Sales;
    public $NetAdjOther;
    public $GainOrLoss;
    public $EndMarketValue;
    public $IncomeDRP;
    public $IncomeCash;
    public $IncomeNotYetPaid;
    public $TotalIncome;
    public $TaxCredits;
    public $TotalReturn;
    public $PercentTotalReturn;
    public $PercentCapitalReturn;
    public $PercentIncomeReturn;    
    
    
    public function savePortfolio($returnHeader,$portfolioId)
    {        
        $portfolios = parent::getArrayFrmJson($returnHeader->data);
        
        extract($portfolios);
        extract($ResponseData);       
        
        if(array_key_exists('Investments',$ResponseData)){
            
            extract($Investments);
            
        } else {
            
            return;
        }
        
                
        $this->portfolioid = $portfolioId['InternalID'];
        $this->FromDate = $ResponseData['FromDate'];
        $this->ToDate = $ResponseData['ToDate'];
        
        $column = parent::columnFetch('11','');           // Coloumn names of table to be fill
        //print_r($column);exit;
        $colcount = count($column);
                
        parent::opendb();
        
        foreach($Investments as $Investment){
                
                for($i=0; $i<$colcount; $i++){
            
                    $this->$column[$i] = addslashes($Investment[ $column[$i] ]);
            
                }
                
                $this->saveInvestmentMovements($returnHeader->error);                

        }
        
        parent::closedb();
        
    }
        
    public function saveInvestmentMovements($qselect){        
        
        if($qselect){
        
            $insert = "INSERT INTO holding_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            // Fetching all coloumn name
            $coloumn = parent::columnFetch('11','');

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key And Related Dates
                
            array_unshift($values,$this->portfolioid,$this->FromDate,$this->ToDate);
            array_unshift($coloumn,'portfolio_id','FromDate','Todate');


           $insert = "INSERT INTO get_investment_movements 
                         (" . implode(', ', $coloumn) . ") ". "VALUES ('" . implode("', '", $values) . "')";
            
        }

        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }
    }

}

?>