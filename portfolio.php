<?php

class portfolio extends utilities
{
    public $InternalID;
    public $PortfolioID;
    public $PortfolioName;
    public $ConsolidatedPortfolioType;
    public $Status;
    public $RecalcType;
    public $RecalcStatus;
    public $ProcessedTo;
    
    //Object variables for child,valuation and detail
    
    public $switcher;
    public $childPortfolio;
    public $portfolioDetails;
    public $portfolioValuation;
    public $portfolioHolding;
    public $AssetClassSummary;
    
    public function __construct(){
       
        $this->switcher           = new switcher();
        $this->childPortfolio     = new ChildPortfolio();
        $this->portfolioDetails   = new PortfolioDetail();
        $this->portfolioValuation = new PortfolioValuation();
        $this->AssetClassSummary  = new GetAssetClassSummary();
        
    }
    
    public function fetchportfolio($returnHeader){              //To fetch all portfolios
             
        $portfolios = parent::getArrayFrmJson($returnHeader->data);   print_r($returnHeader->data);exit;   

        extract($portfolios);    
        extract($ResponseData); 
        
        $column = $this->columnFetch();           // Coloumn names of table to be fill
        $colcount = count($column);//print_r($Portfolios);exit;
                        
        parent::opendb();


       foreach($Portfolios as $Portfolio){
            
                //print_r($Portfolio);exit;
                            
                for($i=0; $i<$colcount; $i++){
          
                    $this->$column[$i] = addslashes($Portfolio[ $column[$i] ]);
            
                }
                
                $this->saveportfolio($returnHeader->error);
                
                if( "Transaction" == $Portfolio['ConsolidatedPortfolioType'])         // Child insertion function
                {
                    $this->switcher->portfolioFetc("other",$Portfolio,"0");
                }
                
                $this->switcher->portfolioFetc("other",$Portfolio,"5");      // Summery insertion function
                         
                $this->switcher->portfolioFetc("other",$Portfolio,"1");      // Detail insertion function
                
                $this->switcher->portfolioFetc("other",$Portfolio,"2");      // valuation insertion function
                
                $this->switcher->portfolioFetc("other",$Portfolio,"8");      // Holdings Excluded insertion function
           
                $this->switcher->portfolioFetc("other",$Portfolio,"12");     // Unrealised CGT Summery insertion function
                
                //$this->switcher->portfolioFetc("childportfolios",$Portfolio,"13");     // Unrealised CGT insertion function
                
                
        } 

        parent::closedb();
    }

    
    
    public function saveportfolio($qselector){  
        
        $coloumn = $this->columnFetch();
        
        //Storing all values in array
        foreach($coloumn as $key){
            
            $values[] = $this->$key;
        }
        
        if($qselector){
            
          $insert = "INSERT INTO portfoliopending (portfolio_id) VALUES ('".$this->InternalID."')";
            
        } else {
            
         $insert = "INSERT INTO portfolio (" . implode(', ', $coloumn) . ") "
                    . "VALUES ('" . implode("', '", $values) . "')";
            
        }
             
        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry !! Some Error Occured";
        }           
            
    }
    
    public function  columnFetch(){

        $json = parent::readConfigJson();
        
        extract($json);
        extract($config);        
        extract($portfolio);
        foreach($portfolio as $column);
        
        return($column);

    } 
}


?>
