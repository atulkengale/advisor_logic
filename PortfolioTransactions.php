<?php


class PortfolioTransactions extends utilities {
    
    public $Asset;
    public $AssetDescription;
    public $ExchangeCode;
    public $TransactionDate;
    public $SettlementDate;
    public $TransactionType;
    public $Quantity;
    public $AveragePrice;
    public $NetAmount;
    public $Unconfirmed;
    public $CostBaseUnknown;
    public $DisposalMethod;
    public $Narration;
    public $BrokerageIncGST;
    public $BrokerageGST;
    public $GSTClaimable;
    public $Broker;
    public $TradedCurrency;
    public $TradedCurrencyAmount;
    public $TradedCurrencyAveragePrice;
    public $SettlementCurrency;
    public $SettlementCurrencyAmount;
    public $SettlementCurrencyAveragePrice;
    public $BuyBack;
    public $DeemedAmount;    
    
    public function savePortfolio($returnHeader,$portfolioId)
    {        
        $portfolios = parent::getArrayFrmJson($returnHeader->data);
        
        extract($portfolios);
        extract($ResponseData);
        
        if(array_key_exists('Transactions',$ResponseData)){
            
            extract($Transactions);
            
        } else {
            
            return;
        }               
        
        $this->portfolioid = $portfolioId['InternalID'];
        
        $column = parent::columnFetch('3','');           // Coloumn names of table to be fill        
        $colcount = count($column);
                
        parent::opendb();
        
        foreach($Transactions as $Transaction){
                
                for($i=0; $i<$colcount; $i++){
            
                    $this->$column[$i] = addslashes($Transaction[ $column[$i] ]);
            
                }
                
                $this->saveTransactionDetail($returnHeader->error);                

        }
        
        parent::closedb();
        
    }
        
    public function saveTransactionDetail($qselect){        
        
        if($qselect){
        
            $insert = "INSERT INTO transaction_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            // Fetching all coloumn name
            $coloumn = parent::columnFetch('3','');

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key
            array_unshift($values,$this->portfolioid);
            array_unshift($coloumn,'portfolio_id');

            $insert = "INSERT INTO transactions
                        (" . implode(', ', $coloumn) . ") ". "VALUES ('" . implode("', '", $values) . "')";

            
        }

        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }
    }
    
    
}


?>
