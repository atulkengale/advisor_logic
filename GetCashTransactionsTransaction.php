<?php

class GetCashTransactionsTransaction extends utilities{

    public $TransactionDate;
    public $Currency;
    public $TransactionType;
    public $Narration;
    public $Deposit;
    public $Withdrawal;
    public $ValuePortfolioCurrency;
    public $Balance;
    public $GLCode;
    
    
    public function savePortfolio($returnHeader,$portfolioId)
    {        
        $portfolios = parent::getArrayFrmJson($returnHeader->data);
        
        extract($portfolios);
        extract($ResponseData);
        
        if(array_key_exists('CashBooks',$ResponseData)){
            
            extract($CashBooks); //print_r($AssetClasses);
            
        } else {
            
            return;
        }
        
        
        if(count($CashBooks) > 0){
            
            //print_r($CashBooks);exit;
        
            foreach($CashBooks as $CashBook){
                
                $this->portfolioid = $portfolioId['InternalID'];
        
                $column = parent::columnFetch('4','GetCashTransactionsTransaction');      // Coloumn names of table to be fill        
                $colcount = count($column);
                
                foreach($CashBook['Transactions'] as $transaction){
                
                    for($i=0; $i<$colcount; $i++){

                        $this->$column[$i] = addslashes($transaction[ $column[$i] ]);

                    }

                    parent::opendb();

                    $this->saveTransactionDetail($returnHeader->error);  

                    parent::closedb();
                
                }
                
            }
        } else {
        
            return;
        }        
        
    }
        
    public function saveTransactionDetail($qselect){        
        
        if($qselect){
        
            $insert = "INSERT INTO transaction_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            // Fetching all coloumn name
            $coloumn = parent::columnFetch('4','GetCashTransactionsTransaction');

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key
            array_unshift($values,$this->portfolioid);
            array_unshift($coloumn,'portfolio_id');



          $insert = "INSERT INTO 
                          get_cash_transactions_transaction 
                          (" . implode(', ', $coloumn) . ") ". "VALUES ('" . implode("', '", $values) . "')";
            
        }

        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }
    }
    
}