<?php


class DB extends utilities {

    private $conn;
    private $status;
    private $type;
    private $IP;
    private $username;
    private $password;
    private $database;

    public function __construct($databasetype){

        $json = parent::readConfigJson();

        extract($json);extract($config);extract($DB);

        $database = $DB[$databasetype];

        $this->IP       = $database['IP'];
        $this->username = $database['username'];
        $this->password = $database['password'];
        $this->database = $database['database'];
    }


    public function opendb($databasetype) {

        switch($databasetype){

            case 0 :

                $this->conn = mysqli_connect($this->IP,$this->username,$this->password,$this->database);
                // Check connection
                if (mysqli_connect_errno()){
                  echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }

                break;

            case 1 :

                $m = new Mongo();
                //echo "Connection to database successfully";
                // select a database
                $this->conn = $m->this->database;
                //echo "Database".$database['database']."selected";
                break;

            default:

                echo "Sorry !! No such Database Exists";

        }

        return($this->conn);

    }

    public function closedb($databasetype,$connection){

        switch($databasetype){

            case 0 :

                mysqli_close($connection);

                break;

            case 1 :


                // select a database
                $this->conn = $m->$this->database;
                //echo "Database".$database['database']."selected";
                break;

            default:

                echo "Sorry !! No such Database Exists";

        }

    }

    public function insertIntoDb($connection,$insertQuery,$databaseType){

        switch($databaseType){

            case 0:

                mysqli_query($connection,$insertQuery);      // insert Query

                break;

            case 1:

                $collection = $connection->mycol;   //selection of Collection
                $document   = $insertQuery;         // Document to be insert

                $collection->insert($document);     //Insertion Query


        }

    }


}
