<?php

include 'errorDatahandler.php';

$url = "https://api.praemium.biz/public/api/Tax/UnrealisedCGT";

$xml = "<GetUnrealisedCGT><InternalID>164281</InternalID></GetUnrealisedCGT>";

$header = array('Token: 1012-8a58a390-dc5b-4f46-8069-53d7495d1a43-635729629064829233','Content-Type:application/xml');

function getportfolios($url,$xml,$header){
        
        $errorhandler = new errorDatahandler();
    
        $httpRequest = curl_init();

        curl_setopt($httpRequest, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($httpRequest, CURLOPT_HTTPHEADER,$header);

        curl_setopt($httpRequest, CURLOPT_POST, 1);

        curl_setopt($httpRequest, CURLOPT_HEADER, 1);

        curl_setopt($httpRequest, CURLOPT_SSL_VERIFYPEER, false);    

        curl_setopt($httpRequest, CURLOPT_URL, $url);

        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, $xml);
        //print_r($httpRequest);

        if( ! $errorhandler->data = curl_exec($httpRequest)) 
        { 
            trigger_error(curl_error($httpRequest));
        }
        if( stripos($errorhandler->data,'Server Error') ) {
            //Use macros
            $errorhandler->error = 1;
            
        } else {
            
            $errorhandler->error = 0;
        }
                
        curl_close($httpRequest);    
        
        return $errorhandler;
    
    }

$returnval = getportfolios($url,$xml,$header); print_r($returnval);



?>