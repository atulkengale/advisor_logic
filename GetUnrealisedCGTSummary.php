<?php

class GetUnrealisedCGTSummary extends utilities{
    
    public $portfolioid;
    public $ValuationDate;    
    
    public $CostBaseUnknown;
    public $ActualCost;
    public $AdjustedCost;
    public $MarketValue;
    public $GrossGain;
    public $DiscountedGain;
    public $OtherGain;
    public $CGTGain;
    public $CGTLoss;   
    
    public function savePortfolio($returnHeader,$portfolioId){ 
        
        $portfolios = parent::getArrayFrmJson($returnHeader->data);
        
        extract($portfolios);
        extract($ResponseData);       
        
        if(array_key_exists('UnrealisedCGT',$ResponseData)){
            
            extract($UnrealisedCGT);
            
        } else {
            
            return;
        }        
                
        $this->portfolioid = $portfolioId['InternalID'];
        $this->ValuationDate = $ResponseData['ValuationDate']; 
        
        
        $column = parent::columnFetch('12','');           // Coloumn names of table to be fill
        //print_r($column);exit;
        if( !array_key_exists('OtherGain',$UnrealisedCGT) ){
            
            unset($column[6]);
            $column = array_values($column);
        }
        $colcount = count($column);       
                
        parent::opendb();       
                
        for($i=0; $i<$colcount; $i++){
            
            $this->$column[$i] = addslashes($UnrealisedCGT[ $column[$i] ]);
            
        }
                
        $this->saveUnrealisedCGT($returnHeader->error);       
        
        parent::closedb();
        
    }
        
    public function saveUnrealisedCGT($qselect){        
        
        if($qselect){
        
            $insert = "INSERT INTO holding_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            // Fetching all coloumn name
            $coloumn = parent::columnFetch('12','');

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key And Related Dates
                
            array_unshift($values,$this->portfolioid,$this->ValuationDate);
            array_unshift($coloumn,'portfolio_id','ValuationDate');


            $insert = "INSERT INTO get_unrealised_CGT_summary 
                         (" . implode(', ', $coloumn) . ") ". "VALUES ('" . implode("', '", $values) . "')";
            
        }

        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }
    }



}

?>