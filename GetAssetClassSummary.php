<?php

class GetAssetClassSummary extends utilities{

    public $AssetClassDescription;
    public $Value;
    public $PercentTotalValue;
    public $PercentNetValue;
    public $MinTargetAllocation;
    public $MaxTargetAllocation;
    public $MidPointTargetAllocation;
    public $Variance;

    public function savePortfolio($returnHeader,$portfolioId){
                
        $portfolios = parent::getArrayFrmJson($returnHeader->data);
        
        extract($portfolios);
        extract($ResponseData);
        
        if(array_key_exists('AssetClasses',$ResponseData)){
            
            extract($AssetClasses); //print_r($AssetClasses);
            
        } else {
            
            return;
        }
               
        
        if(!empty($AssetClasses)){
        
            foreach($AssetClasses as $AssetClasse){
            
            $this->portfolioid = $portfolioId['InternalID'];                             // id of portfolio

            $column = parent::columnFetch('5','GetAssetClassSummary');           // Coloumn names of table to be fill
            $colcount = count($column);         

            for($i=0; $i<$colcount; $i++){

                $this->$column[$i] = addslashes($AssetClasse[ $column[$i] ]);

            }

            parent::opendb();

            $this->saveAssetClass($returnHeader->error);  

            parent::closedb();

            }
        
        } else {
        
            return;
        }
        
    }    
    
        
    public function saveAssetClass($qselect){       
        
        if($qselect){
        
            $insert = "INSERT INTO asset_classes_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            // Fetching all coloumn name
            $coloumn = parent::columnFetch('5','GetAssetClassSummary');

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key
            array_unshift($values,$this->portfolioid);
            array_unshift($coloumn,'portfolio_id');


            $insert = "INSERT INTO asset_classes (" . implode(', ', $coloumn) . ") "
                        . "VALUES ('" . implode("', '", $values) . "')";   
            

        }          
        
        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }
        
    } 
    
}


?>
