<?php

class GetPerformance extends utilities {

    public $FromDate;
    public $ToDate;
    public $PerfInceptionDate;
    public $HoldingsExcluded;
    public $PerfCalcMethod;
    public $BMV;
    public $EMV;
    public $NetCapitalMovement;
    public $NetInternalTransfers;
    public $CashExpenses;
    public $UnallocatedAmount;

    //public $DollarReturn;
    public $CapitalAppreciation;
    public $TotalIncome;
    public $TaxCredits;
    public $ForexMovements;
    public $GrossDollarReturn;
    public $TotalExpenses;
    public $NetDollarReturn;
    
    public $PeriodDescription;
    public $GrossPercentReturn;
    public $NetPercentReturn;
    
    
    public function savePortfolio($returnHeader,$portfolioId){
                
        $portfolios = parent::getArrayFrmJson($returnHeader->data);
        
        extract($portfolios);
        extract($ResponseData);
        
        if(array_key_exists('Performance',$ResponseData)){
            
            extract($Performance);  //print_r($Performance['PercentReturns']);exit; 
            
        } else {
            
            return;
        }
                
        $this->portfolioid = $portfolioId['InternalID'];               // id of portfolio
        
        $column = parent::columnFetch('6','');           // Coloumn names of table to be fill
        $colcount = count($column);         
        
        parent::opendb();
        
        for($i=0; $i< 11; $i++){  
             
               $this->$column[$i] = addslashes($Performance[ $column[$i] ]); 
                
        }
        
        $this->savePerformance($returnHeader->error);           // Saves Performance
        
        
        
        if( count($Performance['DollarReturn']) >0 ){
            
            for($i=11; $i< 18; $i++){

                $this->$column[$i] = addslashes($Performance['DollarReturn'][ $column[$i] ]);

            }
            
        }
        
        $this->saveDollarReturn($returnHeader->error);          //saves dollerreturn
        
        
        
        if( count($Performance['PercentReturns']) >0 ){
                
            foreach($Performance['PercentReturns'] as $percentReturn){
                        
                for($i = 18; $i < 21; $i++){

                    $this->$column[$i] = addslashes($percentReturn[ $column[$i] ]);
                }

                $this->savePercentReturn($returnHeader->error);         //saves percentreturns
                        
            }
                    
        }
        
        parent::closedb();

    }    
    
        
    public function savePerformance($qselect){       
        
        if($qselect){
        
            $insert = "INSERT INTO valuation_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            // Fetching all coloumn name
            $coloumn = parent::columnFetch('6',''); 
            $coloumn = array_slice($coloumn,0,10);

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key
            /*array_unshift($values,$this->portfolioid);
            array_unshift($coloumn,'portfolio_id');*/


            $insert = "INSERT INTO performance (" . implode(', ', $coloumn) . ") "
                        . "VALUES ('" . implode("', '", $values) . "')";   


        }          
        
        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }
        
    } 
    
    public function saveDollarReturn($qselect){       
        
        if($qselect){
        
            $insert = "INSERT INTO valuation_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            // Fetching all coloumn name
            $coloumn = parent::columnFetch('6',''); 
            $coloumn = array_slice($coloumn,11,17);
            $coloumn = array_slice($coloumn,0,7);

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key
            array_unshift($values,$this->portfolioid);
            array_unshift($coloumn,'portfolio_id');


            $insert = "INSERT INTO dollar_return (" . implode(', ', $coloumn) . ") "
                        . "VALUES ('" . implode("', '", $values) . "')";   

        }          
        
        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }
        
    } 
    
    public function savePercentReturn($qselect){       
        
        if($qselect){
        
            $insert = "INSERT INTO valuation_pending (portfolio_id) VALUES ('".$this->portfolioid."')";
            
        } else {
            
            // Fetching all coloumn name
            $coloumn = parent::columnFetch('6',''); 
            $coloumn = array_slice($coloumn,18,20);

            //Storing all values in array
            foreach($coloumn as $key){

                $values[] = $this->$key;
            }

            //Completing both arrays with primary key
            array_unshift($values,$this->portfolioid);
            array_unshift($coloumn,'portfolio_id');


           $insert = "INSERT INTO percent_returns (" . implode(', ', $coloumn) . ") "
                        . "VALUES ('" . implode("', '", $values) . "')";   

        }          
        
        if(!mysqli_query($this->con,$insert)){
        
            echo "Sorry Some Error Occured";
        }
        
    } 

}
